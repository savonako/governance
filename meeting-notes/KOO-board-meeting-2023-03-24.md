Present: Daniel, Lukas, Neal, Ola, Vincent
Absent: 
Absent with excuses:

# Agenda

* Specify board rules regarding absences
* Discuss action items from previous meeting and their progress
* Operations team report
* Discuss Daniel's KOO certificate signing KEP


# Specify board rules regarding absences
* Clarify how to handle the meeting in case someone is absent and add that to the rules

# Action Items from previous meeting

- [ ] Lukas: look into available CoC's and talk to people who might have more experience
- [ ] Vincent: document simple process for joining the election body
- [ ] Vincent: send out voting body invites
    - [ ] Neal: send invites to the individual members we would like to add to the voting body
- [ ] Write introductory item about the Board on KOO
  - At the openpgp email summit decided to move to a community-based governance model
  - First election was in Nov
  - New board consisting of xxx first convened in Dec
  - Looking for people to join the election body (link to Vincent's post)
  - The server is running nicely without major issues
  
  - Vincent on tone:
    - wants the blog to be approachable to end users (not technical; no talk about specs)
    - Emoji or two at the end, but not all over
- [x] Daniel: Formulate the process how to request a rate limit increase


# Meeting with Expert about our privacy policy
* Discuss if fingerprint is PII
* How can we change the privacy policy to better handle that
* Can public good be argued / would that help


# Rate Limit request from Proton
* Accepted
* Vincent will raise to 100r/s


# Operations team report
* Nothing to report
* No new support requests


# Discuss Daniel's KOO certificate signing KEP
* see https://pad.riseup.net/p/3v1qrWKh9TntuLhKY9Dp-keep

## Major decision point seems to be: Request Signing vs Certifying User IDs

* Request Signing helps for caching requests and verifying they came from KOO
** Very different in terms of tech stack from OpenPGP
** Options might be https://www.ietf.org/archive/id/draft-ietf-httpbis-message-signatures-04.html, https://web.dev/signed-exchanges/, or our own thing
** would likely be fairly Proton-specific

* Certifying User IDs via OpenPGP
** easier to integrate into openpgp-based trust decisions and models
** higher commitment, not as easy to revert
** unclear how to deal with revocations? they could be stripped, since certificates are not signed as a whole

* "Hybrid" solution might be to attach a packet to the OpenPGP certificate that signs it fully

## Core decision: Should OpenPGP be a CA?

* KOO is already used as "de facto" CA in many places
* Vincent: still big change in mission statement and strategy

* Individual opinions:
** Vincent: I would prefer to stick to KOO being a key distribution and discovery mechanism
** Neal: KOO would be more useful if it signed user ids for openpgp-based authentication models, and is anyways already a de facto CA
** Ola: Ideally, KOO would be limited to key distribution only, and leave verification/discovery to a different service.

* Vincent: Our current board constellation does not reflect becoming a CA strongly enough that we should consider it our mandate without asking the community for feedback. So, let's do that.

-> Discussion continues next time.

# Actions items for next meeting on 

- [ ] Lukas: look into available CoC's and talk to people who might have more experience
- [ ] Vincent: document simple process for joining the election body
- [ ] Vincent: send out voting body invites
- [ ] Neal: send invites to the individual members we would like to add to the voting body
- [ ] Write introductory item about the Board on KOO
  - At the openpgp email summit decided to move to a community-based governance model
  - First election was in Nov
  - New board consisting of xxx first convened in Dec
  - Looking for people to join the election body (link to Vincent's post)
  - The server is running nicely without major issues
  
  - Vincent on tone:
    - wants the blog to be approachable to end users (not technical; no talk about specs)
    - Emoji or two at the end, but not all over
- [ ] Neal: Invite Expert to talk about GDPR and keyservers. Either next regular meeting or based on offer by Expert (on UK time)
- [ ] Vincent + Lukas: discuss and document the single components of the current server infrastructure and tasks the operations team is – and future members would be – responsible for.
- [ ] Daniel: Write an abstract version of the rate limiting process – what information to include, point of contact – based on the rate limiting request from Daniel for Proton
- [ ] Vincent: increase rate limit as per proton's request
