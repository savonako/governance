Summary of the keys.openpgp.org Constitution
===

High Level Summary
---

keys.openpgp.org (KOO) is a service providing a verifying key server to the OpenPGP
ecosystem.
The service is operated by the operations team as guided by the
Board.
The Board is elected by the Voting Body, which is formed by individuals
that are active in the OpenPGP ecosystem.


The Board
---

The Board offers advice, guidance, and support to the operations
team, and helps ensure the ongoing operation of the KOO service.
If and when the KOO organization gets funds, the Board decides how
to spend them.
The Board consists of 3-5 individuals.
Board members are elected for a 1-year term, and may be on the Board
for up to 3 years in a row.
Board votes are decided by simple majority, except when replacing
the whole operations team, which must be a supermajority vote in the board.

The Board nominates one of its members as secretary.
The secretary takes meeting minutes and organizes the next election.
Board meeting minutes are published.

The Board takes care of KOO Enhancement Proposals (KEP) that may be
submitted by any voting member.
Any KEP requires adoption by at least one Board member in order to
be considered by the Board.
The Board may approve or reject any KEP under consideration, or may
ask the KEP author for revisions before re-consideration.

Board members self-nominate themselves via a public mailing list.
Elected members are asked to ensure that no organization or affiliation
is over-represented in the Board.


The Voting Body
---

The voting body serves to elect the Board Members. It consists of voting
members.
Eligible for membership are all those individuals who
use OpenPGP, implement it, provide services to help use it,
produce documentation, provide training, etc.
Voting members are nominated by existing members and approved by the Board.
Membership expires after 3 years of inactivity (defined by participating
in the votes and elections).

Membership in the initial voting body is open to anyone who has attended any of the past
[OpenPGP E-mail Summits](https://wiki.gnupg.org/OpenPGPEmailSummits).
This only applies to the election of the first Board.


The Operations Team
---

The operations team maintains the Hagrid software, and operates the
servers providing the service of the key server.
It has final say in how the software works, and how the service is provided.
The operations team reports on their activities to the Board
and the public.
The operations team is self-organized, except for the right of the Board to
replace the operations team entirely.


Initial Formation of the KOO Organization
---

The KOO Bootstrap Committee will organize the process to establish the
KOO organization as follows:
1. Request for feedback from the OpenPGP community (public announcement).
1. Incorporate the community feedback and publish the 1st KOO Constitution.
1. Invite attendees of the past [OpenPGP E-mail Summits](https://wiki.gnupg.org/OpenPGPEmailSummits)
   to join the Voting Body.
1. Organize the election of the first Board.
1. The constitution is considered ratified once the 1st elected Board ins installed.

In order to ensure continuity, 2 of the 5 initial Board members will have a
term limit of max. 2 years.

Voting Process
---

Voting and elections are done publicly and are attributable.
Votes for Board elections are done by signed commits via merge
requests on a dedicated git repository.

Changing the Constitution
---

The constitution may be changed by voting by the Voting Body. To
pass, a change must receive at least 67% of the votes given, and at
least 50% of the eligible voters must vote.
