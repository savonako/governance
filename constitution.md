# The keys.openpgp.org Constitution

## High level summary

* KOO is a service providing a verifying key server to the OpenPGP
  ecosystem.
* The service is operated by the operations team as guided by the
  Board.
* The Board is elected by the "KOO organization", a group of people
  and organizations active in the OpenPGP ecosystem.

## The operations team

* Under the guidance of the Board, the operations team maintains the
  Hagrid software, and operates the servers providing the service of
  the key server.
  - _Rationale:_ The operations team is responsible for maintaining
    the service, as they have been before this constitution was
    adopted.  The type of technical work may need to change, for
    example, to use other software to implement the service.
    Substantial changes here may mean changing the entire nature of
    the service.
* The operations team is committed to following the guidance of the
  Board.
  - _Rationale:_ The Board has decision-making authority, and the
    operations team defers to that authority.
* The operations team has final say in how the software works, and the
  service is provided.
  - _Rationale:_ This acknowledges the reality that those with
    operational access _can_ do what they want. For the health of the
    ecosystem, this power needs to be balanced though transparency.
* The operations team shall report on their activities to the Board
  and the public without unnecessary delay. To the extent that the
  operations team is unable to follow any Board guidance, this
  divergence and the reason for it shall be included in the reports in
  addition to any activity. The operations team and the Board shall
  negotiate and agree on the level of detail of the reporting, and the
  mechanism by which it is done.
  - _Rationale:_ The reporting provides transparency to the Board, the
    voting body, and the general public. However, the level of detail
    and mechanisms may change over time, and it's not particularly
    important what they are, as long as reporting happens.
* All reports from the operations team to the Board shall be made
  public by default. Some reports may contain sensitive data that, for
  example, may affect the safe and secure operation of the KOO
  service. That sensitive data should be redacted before releasing the
  report to the public. Once the redacted data is no longer sensitive,
  the public report should be unredacted.
  - _Rationale:_ The Board needs to know what is going on in the
    service to be able to provide reasonable guidance.  And fully
    public reports are important for transparency. However, the
    operations team and the Board should be aware of the need for a
    time-limited secrecy in some contexts, and have means at their
    disposal to act responsibly.

## The Board

* The Board offers advice, guidance, and support to the operations
  team, and helps ensure the ongoing operation of the KOO service.
  - _Rationale:_ The operations team does things. The Board makes sure
    they do what's needed, and that the service operates in a way that
    serves the ecosystem.
* If and when the KOO organization gets funds, the Board decides how
  to spend them.
  - _Rationale:_ For now, KOO has no funds, and operates on donated
    resources. If this changes, the rules for KOO may need to change
    as well. For now, it doesn't seem worth worrying about it.
    Instead, have rules that allow a transparent decision making
    process.
* The Board consists of at most five individuals.
  - _Rationale:_ An odd number would reduce ties in Board votes. Five
    seems enough for the current scale of operations. Could be seven
    too. Too small a board means too much work and responsibility per
    board member; too large a board would make it too easy to shirk
    responsibility.
* When elected, a Board serves for one year (365 or 366 days). A Board
  election should be organized by the secretary and carried out before
  the year is over. If no Board election has been held by the end of
  one year, the Board has no other business than organizing and
  holding a Board election.
  - _Rationale:_ This gives the Board legitimacy through reasonably
    frequent elections. Also, committing to one year on the Board
    seems fairly easy, whereas committing to, say, three years, would
    be harder for many people.
* Board membership is personal, and a Board member does not represent
  their employer or other affiliations.
  - _Rationale:_ We want people to be able to act in their own
    interests, not in the interests of their employer.  It'd be
    problematic for the Board to be dominated by one company, but
    trying to mechanically prohibit such a takeover adds too much
    complexity to the constitution.
* The Board makes decisions primarily by consensus. If any Board
  member requests it, a specific decision is made by voting instead. The
  Board member requesting a vote must clearly state the topic of the
  vote, in writing, as something that can either be approved or
  rejected.  Voting may be done by any means that suits the whole
  board. If they do not decide otherwise, a show of hands in an
  in-person meeting or video conference is used.
  - _Rationale:_ Consensus is a good way to govern, but it must be
    explicit. If there is no consensus, or someone wants a vote for
    clarity, voting must also be used.
* Board votes are decided by simple majority, except when replacing
  the whole operations team, which must be unanimous by all members.
  There is no tie-breaking vote: if a simple majority is not achieved,
  the proposed decision is not approved.
  - _Rationale:_ A simple majority seems enough for a small Board for
    most things. A radical change should be unanimous.
* The Board may nominate one of its members for removal by unanimous
  agreement of the rest of the Board so long as the nominated member
  does not dissent.  The Board member nominated for removal has one
  week from nomination to register their dissent.
  - _Rationale:_ An ill member or someone who isn't participating
    can't prevent the Board from working.
* A Board member is expected to recuse themself from any vote or
  decision where they have a conflict of interest.
  - _Rationale:_ If someone doesn't recuse themself when they should
    have, the voting body can simply wait for their term to expire and
    not bring them back in the next Board election.  In a particularly
    egregious case, other members of the board could resign en masse
    to force a new Board election immediately.  No more complex
    mechanism is needed.
* If a Board member leaves the Board, the remaining Board will serve
  the remaining term.
  - _Rationale:_ It doesn't seem worth forcing a new Board election
    when one person steps down.
* If the remaining Board has 2 or fewer people, the Board is dissolved
  and a new Board election is held.  A Board election triggered by
  such a collapse is organized by the operations team.
  - _Rationale:_ A Board with 2 people indicates a loss of confidence
    in the legitimacy of the Board, and should be addressed by
    starting from scratch.  The ops team organizes this election
    because there is no secretary after the Board is dissolved.
* A new Board's first order of business is to choose one of its
  members to be the secretary. This choice must be unanimous.
  - _Rationale:_ The secretary needs to be trusted, so a unanimous
    choice seems worth it. Also, in a small Board, it shouldn't be
    difficult to achieve. At the same time, the secretary doesn't
    actually have much power.
* The secretary may be changed by unanimous decision by the board.
  - _Rationale:_ This is needed, for example, if the secretary leaves
    the Board, or if the secretary wants to hand over the position of
    secretary to another willing Board member.
* Any board member can adopt a [KOO Enhancement
  Proposal](KEP-outline.md) by identifying at least one author for the
  KEP from the voting members. Such an adopted KEP gets assigned a
  number for reference and the Board will consider it. The adopting
  board member is the shepherd for that KEP. The Board may decide to
  ask the KEP author(s) for revisions, to explicitly approve it as
  guidance for KOO, or to explicitly reject it. If the document
  shepherd leaves the Board before a clear final outcome, the KEP will
  be dropped from the Board agenda unless adopted by another Board
  member.
  - _Rationale:_ We want a clear way for the community to engage with
    the Board.  We also want a clear log of what is being discussed,
    and an incentive to actually make decisions in a reasonable time
    frame about a KEP, so that it doesn't linger.
* Ongoing Board policies, practices, and procedures are collected and
  maintained in [a board policy document](board-policies.md). The KOO
  Board follows the guidance outlined that document and may also
  update it directly through standard Board operations to reflect
  current best practice. If that document ever conflicts with this
  Constitution, the Constitution takes precedence.
  - _Rationale:_ This Constitution is high-level, and should not
    prescribe unnecessarily-specific details for the Board.  At the
    same time, the Board's commitments to transparency,
    accountability, and continuity require that it maintain a clear
    explanation of how it functions.

### The Secretary of the Board

* The secretary organizes all votes for the board, if any, and the
  election for the next board, and any votes for the voting body.
  - _Rationale:_ This is the primary responsibility of the secretary.
    Votes are a crucial factor in giving the Board and KOO legitimacy
    to the ecosystem.
* The secretary will also keep minutes of Board meetings. Minutes are
  published openly and promptly.
  - _Rationale:_ This is probably most of what the secretary needs to
    do, and it's a crucial part of the transparency required to run a
    trustworthy service.

## The voting body

* Voting body serves to elect the Board Members. It consists of voting
  members.

* The voting body represents the whole OpenPGP ecosystem: those who
  use OpenPGP, or implement it, provides services to help use it,
  produce documentation, provide training, etc.
  - _Rationale:_ This representation is required to give the KOO
    service legitimacy to the ecosystem.
* Eligible for membership are all those individuals who are active in
  helping others use OpenPGP.
  - _Rationale:_ The Board evaluates whether this is true for any
    proposed member.
* An existing member may nominate new members.
  - _Rationale:_ It should be easy for anyone active in the ecosystem
    to find an existing member to nominate them.
* The Board approves new members.
  - _Rationale:_ The Board should be able to do this with the least
    hassle. At the same time, having the Board do the approval, rather
    than the voting body at large, makes it a little harder for a
    malicious party to infiltrate the KOO organization by flooding the
    voting body with members who in turn bring in more malicious
    members.
* The voting body may remove a member from itself by simple majority.
  The Board nominates a voting member for removal.
  - _Rationale:_ It shouldn't be too easy to remove a member. Removing
    someone's membership is a drastic measure, and should require a
    full vote.
* A voting member may resign at any time by informing the voting body
  and Board.
  - _Rationale:_ If a member doesn't want to stay as a member, it
    should be easy for them to resign.
* A deceased person will be removed from the voting body by the
  secretary of the Board without voting. If it turns out the removal
  was done in mistake, it is undone without voting.
  - _Rationale:_ Some way of dealing with those who can't resign is
    needed, but mistakes should be easy to undo.
* A voting member who has not voted in any of the votes in the last
  three years shall be removed from the voting body by the secretary
  of the Board, without voting.
  - _Rationale:_ A stagnant voting body is unhealthy.  This clause
    ensures automatically that only people with a bare minimum of
    involvement stay relevant.  A voting member who has been removed
    in this way can always ask another voting member to re-nominate
    them for membership when they're ready to participate again.
* The voting body's initial formation is described in [a separate
  bootstrapping document](bootstrapping.md).

## Voting process

* Every election has a designated organizer, typically the secretary
  of the current Board.
  - _Rationale:_ in some specific cases, such as when the current
    Board has collapsed, or during the bootstrap phase, the operations
    team may be the election organizer.
* For any particular election, the designated election organizer may
  explicitly delegate the organizing responsibilities and obligations
  for that election to any other willing member of the voting body.
  - _Rationale:_ If the organizer's life intervenes, there should be
    some way that other people can help out.
* The list of eligible voters is compiled and published by the
  election organizer.
  - _Rationale:_ This is needed so that votes can be verified.
* All votes are public and attributable.
  - _Rationale:_ This is needed so that votes can be verified.
* For Board elections, voting is done by signed commits via merge
  requests on a dedicated git repository.
  - _Rationale:_ This is needed so that votes can be verified. This
    seems like a simple way of achieving that.
* Standard procedure for votes among the membership is documented in
  [a separate document describing the electoral
  process](electoral-process.md), which the Board may amend by
  standard Board action. If that document ever conflicts with this
  Constitution, the Constitution takes precedence.
  - _Rationale:_ This Constitution is high-level, and should not
    prescribe unnecessarily-specific details about electoral process.
    For transparency, accountability, and continuity, KOO needs to
    maintain a clear explanation and expectations of how a vote will
    function.

## Board elections

* With the exception of term limits (see below), any voting member may
  stand as a candidate for the Board.  A voting member self-nominates
  for the Board by writing to the KOO mailing list, offering whatever
  information they think would be useful for voting body to consider,
  including their vision for KOO and what relevant OpenPGP groups they
  are affiliated with.
  - _Rationale:_ We want anyone part of the community to be able to be
    on the Board.  We also don't want to constitutionally require
    candidates to share specific information, but hope that voting
    members are unlikely to blindly elect candidates that are unable
    or unwilling to describe their relevant background or their vision
    for the project.
* Any member successfully seated on the Board in every Board election
  over the last three years may not stand for the Board in the current
  election.
  - _Rationale:_ It's an anti-pattern for a long-lived community
    project to have the same people in positions of power for extended
    periods of time. Thus, a limit until some change in the Board is
    forced seems sensible. At the same time, it seems worth allowing a
    member to re-join after being off the Board for at least one
    election cycle.
* The ballot in elections shall list any number of candidates the
  voter approves of being on the Board.  Each member of the voting
  body may cast one such ballot per election.
  - _Rationale:_ The Board has many members, and voters should be able
    to show support to many candidates. However, keeping voting simple
    seems worthwhile. A more complicated, but arguably more fair,
    voting system like the Condorcet variant the Debian project uses
    is significantly harder to implement.
* In a Board election, the five candidates with the most approvals,
  and with at least five approvals each will form the new board.
  - _Rationale:_ This meant to be really simple to verify.
* The Board election process starts from an empty Board and a list of
  candidates who each received at least five approvals.  The candidate
  with the most approvals is offered a seat on the Board and removed
  from the list.  The selected candidate may then either accept or
  decline to be seated.  This iterates until the Board has five seated
  members or no candidates remain.
  - _Rationale:_ This is basic "approval voting" with a 5-approval
    minimum.  The minimum ensures that during an election cycle when
    candidates are scarce, a widely-untrusted candidate cannot join
    the board simply by standing.  Some visible support is needed.
* When offered a seat on a new Board, a candidate is expected to
  decline the seat if they feel their affiliations are already
  well-represented on the Board being assembled.
  - _Rationale:_ It is important to ensure that the Board represents
    diverse interests across the OpenPGP ecosystem.  We do not want a
    Board that appears to be controlled by a single company, employer,
    or organization.  At the same time, deciding what constitutes a
    shared affiliation is not easy to adjudicate.  A candidate's
    affiliations and understanding of this clause should be discussed
    during candidacy so voters can make their own choices.
* In the event of a tie when identifying the next candidate to be
  offered a seat on the Board, the tie is broken by random chance
  between the tied candidates.
  - _Rationale:_ approval voting with a relatively small electorate is
    prone to ties. More specific explanations of how we elicit chance
    would be too technical for this document. See [the tie breaking
    document](tie-breaking.md) for an example of how it can work.

## Conflicts within the organization

* The operations team has an effective veto over any change affecting
  the service and how it is run. This includes membership changes of
  the operations team.  The only exceptions are wholesale replacement
  of the operations team and ownership changes to DNS records related
  to the keys.openpgp.org service (the keys.openpgp.org subdomain, the
  main openpgp.org domain, or any additional domain names).
  - _Rationale:_ The operations team should not be forced to do what
    they object to. At the same time, they should not be able to hold
    the service hostage, so giving the Board the authority to control
    DNS entries is a counter-balance.
* The board may, by a supermajority vote, replace the whole operations
  team at once. A supermajority is defined as at least two thirds of
  the board, but no fewer than three board members.  If the old
  operations team is willing to hand over existing infrastructure to
  the new team, service won't be disrupted. However, the new team may
  need to set up new infrastructure, and the Board may point DNS
  entries to that. This will be disruptive to the ecosystem, and the
  Board shall take that into account.
  - _Rationale:_ Replacing the whole operations team at once should be
    possible, but should not be easy.  If a minimal Board (with only
    three members) wants to replace the operations team, they need to
    be unanimous.  Otherwise, the organization permits replacement
    with at most a single dissenting vote.
* Conflict among and between existing Board members is handled by
  standard Board operations.
  - _Rationale:_ While the Board will typically operate in good faith
    on consensus, when a Board is split there are existing mechanisms
    to resolve conflict, like holding a vote.
* If a Board secretary is failing to fulfill their duties in good
  faith and refuses to step aside as secretary, other Board members
  may resign en masse to force a new Board election.
  - _Rationale:_ The role of secretary has the capacity to hamper or
    indefinitely delay matters through bureaucratic malice.  At the
    same time, the role of secretary can't move to a new person
    without the consent of the current secretary.  Mass resignation
    will trigger a new Board election, and the new Board will select a
    new secretary.

## Changing the constitution

* This constitution may be changed by voting by the voting body. To
  pass, a change must receive at least 67% of the votes given, and at
  least 50% of the eligible voters must vote.
  - _Rationale:_ The constitution should not be changed lightly, but
    it should be possible to do so when it's warranted.
* Changes to the constitution may be proposed by any member of the
  voting body, and must be seconded by at least one other member.  At
  least one of the proposer or seconders must be a Board member.
  - _Rationale:_ Anyone should be able to propose a change, but given
    the effort to run a vote, it shouldn't be too easy to force a
    vote.
* Proposed changes must be communicated to the voting body at least 30
  days before the vote happens.
  - _Rationale:_ It's important to tell voters about a vote, and give
    voters time to study and debate that proposed change.
